FROM node:slim

EXPOSE 3000
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci --only=prod

COPY . ./
CMD ["npm", "run", "server"]

