# Convert 5eTools JSON output to DnDAppFile XML

## Installation

Install Node.js. Run:

    $ npm install

## Usage

    $ node index.js input.json | tee output.xml

You can also install the software globally to use it from anywhere.

    $ npm i -g .
    $ 5eTools-to-DnDAppFile input.json
