#!/usr/bin/env node

const fs = require('fs');

const { convert } = require('./converter');

const inputFilename = process.argv[2];

if (!inputFilename) {
    console.log('USAGE: script.js inputfile.json');
    process.exit(1);
}

const inputString = fs.readFileSync(inputFilename, { encoding: 'UTF-8' });
const outputString = convert(inputString);

console.log(outputString);
