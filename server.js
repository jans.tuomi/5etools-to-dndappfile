const express = require('express')
const app = express();
const port = 3000;

const { convert } = require('./converter');

app.use(express.static('public'));
app.use(express.json());

app.post('/convert', (req, res) => {
    const data = req.body;
    const jsonInput = data.json;
    const xmlOutput = convert(jsonInput);
    res.json({
        xml: xmlOutput,
    });
})

app.listen(port, () => console.log(`Example app listening on http://localhost:${port}!`))
